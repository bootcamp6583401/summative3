package summative.three.api.models;

import com.fasterxml.jackson.annotation.JsonBackReference;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

@Entity
public class Score {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private Integer id;

    @Getter
    @Setter
    private int score;

    @ManyToOne
    @JoinColumn(name = "lesson_id", referencedColumnName = "id")
    @JsonBackReference(value = "lesson-scores")
    @Getter
    @Setter
    private Lesson lesson;

    @ManyToOne
    @JoinColumn(name = "student_id", referencedColumnName = "id")
    @JsonBackReference(value = "student-scores")
    @Getter
    @Setter
    private Student student;

}
