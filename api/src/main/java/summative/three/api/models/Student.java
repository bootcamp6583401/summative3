package summative.three.api.models;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.Getter;
import lombok.Setter;

@Entity
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private Integer id;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String surname;

    @Getter
    @Setter
    private LocalDate birthdate;

    @Getter
    @Setter
    @Enumerated(EnumType.STRING)
    private Gender gender;

    public static enum Gender {
        MALE, FEMALE
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "student")
    @JsonManagedReference(value = "student-scores")
    @Getter
    @Setter
    private List<Score> scores;

}
