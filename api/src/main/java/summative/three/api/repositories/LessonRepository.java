package summative.three.api.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import jakarta.transaction.Transactional;
import summative.three.api.models.Lesson;

public interface LessonRepository extends CrudRepository<Lesson, Integer> {
    @Query(value = "INSERT INTO lesson (name, level) VALUES (?1, ?2)", nativeQuery = true)
    @Transactional
    @Modifying
    public void saveCustom(String name, int level);

    @Query(value = "SELECT id, name, level FROM lesson where name = ?1 LIMIT 1", nativeQuery = true)
    public Lesson findOneByName(String name);

    @Query(value = "SELECT id, name, level FROM lesson LIMIT 1", nativeQuery = true)
    public Lesson findOne();

    @Query(value = "SELECT id, name, level FROM lesson", nativeQuery = true)
    public List<Lesson> findAll();
}
