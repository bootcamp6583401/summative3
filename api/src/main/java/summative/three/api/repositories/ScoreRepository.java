package summative.three.api.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import jakarta.transaction.Transactional;
import summative.three.api.models.Score;

public interface ScoreRepository extends CrudRepository<Score, Integer> {
    @Query(value = "INSERT INTO lesson (score, student_id, lesson_id) VALUES (?1, ?2, ?3)", nativeQuery = true)
    @Transactional
    @Modifying
    public void saveCustom(int score, int student_id, int lesson_id);

    @Query(value = "SELECT id, score, student_id, lesson_id FROM score LIMIT 1", nativeQuery = true)
    public Score findOne();

    @Query(value = "SELECT id, score, student_id, lesson_id FROM score", nativeQuery = true)
    public List<Score> findAll();
}
