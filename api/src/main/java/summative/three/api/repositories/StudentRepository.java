package summative.three.api.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import jakarta.transaction.Transactional;
import summative.three.api.models.Student;
import summative.three.api.models.Student.Gender;

import java.time.LocalDate;

public interface StudentRepository extends CrudRepository<Student, Integer> {
    @Query(value = "INSERT INTO student (name, surname, birthdate, gender) VALUES (?1, ?2, ?3, ?4)", nativeQuery = true)
    @Transactional
    @Modifying
    public void saveCustom(String name, String surname, LocalDate birthdate, Gender gender);

    @Query(value = "SELECT id, name, surname, birthdate, gender FROM student where name = ?1 LIMIT 1", nativeQuery = true)
    public Student findOneByName(String name);

    @Query(value = "SELECT id, name, surname, birthdate, gender FROM student LIMIT 1", nativeQuery = true)
    public Student findOne();

    @Query(value = "SELECT id, name, surname, birthdate, gender FROM student", nativeQuery = true)
    public List<Student> findAll();
}
