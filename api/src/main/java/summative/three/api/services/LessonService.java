package summative.three.api.services;

import java.util.List;

import summative.three.api.models.Lesson;

public interface LessonService {
    List<Lesson> getAll();

    Lesson getById(int id);

    Lesson save(Lesson obj);

    void deleteById(int id);

    Lesson updateById(int id, Lesson brand);

}