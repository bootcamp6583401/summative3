package summative.three.api.services.iml;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import summative.three.api.models.Score;
import summative.three.api.repositories.ScoreRepository;
import summative.three.api.services.ScoreService;
import java.util.Optional;

@Service
public class ScoreServiceImpl implements ScoreService {

    @Autowired
    ScoreRepository repo;

    public List<Score> getAll() {
        return repo.findAll();
    }

    public Score save(Score score) {
        return repo.save(score);
    }

    public void saveCustom(Score score) {
        repo.saveCustom(score.getScore(), score.getStudent().getId(), score.getLesson().getId());
    }

    public Score getById(int id) {
        Optional<Score> res = repo.findById(id);
        if (res.isPresent()) {
            return res.get();
        }
        return null;
    }

    public Score updateById(int id, Score score) {

        score.setId(id);
        return repo.save(score);
    }

    public void deleteById(int id) {
        repo.deleteById(id);
    }

}