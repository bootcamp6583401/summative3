package summative.three.api.services.iml;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import summative.three.api.models.Student;
import summative.three.api.repositories.StudentRepository;
import summative.three.api.services.StudentService;

import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    StudentRepository repo;

    public List<Student> getAll() {
        return repo.findAll();
    }

    public Student save(Student student) {
        return repo.save(student);
    }

    public void saveCustom(Student student) {
        repo.saveCustom(student.getName(), student.getSurname(), student.getBirthdate(), student.getGender());
    }

    public Student getById(int id) {
        Optional<Student> res = repo.findById(id);
        if (res.isPresent()) {
            return res.get();
        }
        return null;
    }

    public Student updateById(int id, Student student) {

        student.setId(id);
        return repo.save(student);

    }

    public void deleteById(int id) {
        repo.deleteById(id);
    }

}