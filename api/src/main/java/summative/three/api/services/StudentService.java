package summative.three.api.services;

import java.util.List;

import summative.three.api.models.Student;

public interface StudentService {
    List<Student> getAll();

    Student getById(int id);

    Student save(Student obj);

    void deleteById(int id);

    Student updateById(int id, Student brand);

}
