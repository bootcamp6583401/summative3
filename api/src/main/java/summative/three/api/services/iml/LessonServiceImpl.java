package summative.three.api.services.iml;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import summative.three.api.models.Lesson;
import java.util.Optional;
import summative.three.api.repositories.LessonRepository;
import summative.three.api.services.LessonService;

@Service
public class LessonServiceImpl implements LessonService {

    @Autowired
    LessonRepository repo;

    public List<Lesson> getAll() {
        return repo.findAll();
    }

    public Lesson save(Lesson lesson) {
        return repo.save(lesson);
    }

    public void saveCustom(Lesson lesson) {
        repo.saveCustom(lesson.getName(), lesson.getLevel());
    }

    public Lesson getById(int id) {
        Optional<Lesson> res = repo.findById(id);
        if (res.isPresent()) {
            return res.get();
        }
        return null;
    }

    public Lesson updateById(int id, Lesson lesson) {

        lesson.setId(id);
        return repo.save(lesson);

    }

    public void deleteById(int id) {
        repo.deleteById(id);
    }

}