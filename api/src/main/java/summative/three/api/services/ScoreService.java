package summative.three.api.services;

import java.util.List;

import summative.three.api.models.Score;

public interface ScoreService {
    List<Score> getAll();

    Score getById(int id);

    Score save(Score obj);

    void deleteById(int id);

    Score updateById(int id, Score brand);

}
