package summative.three.api.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import summative.three.api.models.Score;
import summative.three.api.services.iml.ScoreServiceImpl;

@RestController
@RequestMapping("/scores")
public class ScoreController {

    @Autowired
    ScoreServiceImpl service;

    @GetMapping
    public Iterable<Score> getAll() {
        return service.getAll();
    }

    @PostMapping
    public Score save(@RequestBody Score product) {
        return service.save(product);
    }

    @GetMapping("/{id}")
    public Score getById(@PathVariable int id) {
        return service.getById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id) {
        service.deleteById(id);
    }

    @PutMapping("/{id}")
    public Score updateById(@PathVariable int id, @RequestBody Score product) {
        return service.updateById(id, product);
    }

}