package summative.three.api.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import summative.three.api.models.Student;
import summative.three.api.services.iml.StudentServiceImpl;

@RestController
@RequestMapping("/students")
public class StudentController {

    @Autowired
    StudentServiceImpl service;

    @GetMapping
    public Iterable<Student> getAll() {
        return service.getAll();
    }

    @PostMapping
    public Student save(@RequestBody Student product) {
        return service.save(product);
    }

    @GetMapping("/{id}")
    public Student getById(@PathVariable int id) {
        return service.getById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id) {
        service.deleteById(id);
    }

    @PutMapping("/{id}")
    public Student updateById(@PathVariable int id, @RequestBody Student product) {
        return service.updateById(id, product);
    }

}