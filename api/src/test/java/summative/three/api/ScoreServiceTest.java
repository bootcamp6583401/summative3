package summative.three.api;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import summative.three.api.models.Lesson;
import summative.three.api.models.Score;
import summative.three.api.models.Student;
import summative.three.api.repositories.ScoreRepository;
import summative.three.api.services.iml.ScoreServiceImpl;

@SpringBootTest
public class ScoreServiceTest {
    @Mock
    private ScoreRepository repo;

    @InjectMocks
    private ScoreServiceImpl service;

    @Test
    public void whenGetAllScores_thenReturnScoreList() {
        Score obj = new Score();
        obj.setId(1);
        obj.setScore(99);

        when(repo.findAll()).thenReturn(Collections.singletonList(obj));

        List<Score> result = service.getAll();

        assertEquals(1, result.size());
        assertEquals(99, result.get(0).getScore());
    }

    @Test
    public void whenGetScoreById_thenReturnScore() {
        Score obj = new Score();
        obj.setId(1);
        obj.setScore(99);

        when(repo.findById(1)).thenReturn(Optional.of(obj));

        Score result = service.getById(1);

        assertEquals(99, result.getScore());
    }

    @Test
    public void whenNotFound() {

        when(repo.findById(1)).thenReturn(Optional.ofNullable(null));

        Score result = service.getById(1);

        assertNull(result);
    }

    @Test
    public void whenSaveScore_thenScoreShouldBeSaved() {
        Score obj = new Score();
        obj.setId(1);
        obj.setScore(99);

        when(repo.save(any(Score.class))).thenReturn(obj);

        Score savedScore = service.save(obj);

        assertNotNull(savedScore);
        assertEquals(obj.getScore(), savedScore.getScore());
        verify(repo, times(1)).save(obj);
    }

    @Test
    public void customSave() {
        Score obj = new Score();
        obj.setId(1);
        obj.setScore(33);

        Student student = new Student();

        student.setId(1);
        student.setName("student");

        Lesson lesson = new Lesson();

        lesson.setId(1);
        lesson.setName("lesson");

        obj.setLesson(lesson);

        obj.setStudent(student);
        service.saveCustom(obj);

        verify(repo, times(1)).saveCustom(obj.getScore(), obj.getStudent().getId(), obj.getLesson().getId());
    }

    @Test
    public void whenDeleteScore_thenScoreShouldBeDeleted() {
        int objId = 1;

        doNothing().when(repo).deleteById(objId);
        service.deleteById(objId);

        verify(repo, times(1)).deleteById(objId);
    }

    @Test
    public void whenUpdateScore_thenScoreShouldBeUpdated() {
        Score existingScore = new Score();
        existingScore.setId(1);
        existingScore.setScore(95);

        Score updated = new Score();
        updated.setId(1);
        updated.setScore(99);

        when(repo.findById(existingScore.getId())).thenReturn(Optional.of(existingScore));
        when(repo.save(any(Score.class))).thenAnswer(i -> i.getArguments()[0]);

        Score updatedScore = service.updateById(existingScore.getId(), updated);

        assertNotNull(updatedScore);
        assertEquals(updated.getScore(), updatedScore.getScore());
        verify(repo, times(1)).save(updatedScore);
    }

}
