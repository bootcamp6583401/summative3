package summative.three.api;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import summative.three.api.models.Student;
import summative.three.api.models.Student.Gender;
import summative.three.api.repositories.StudentRepository;
import summative.three.api.services.iml.StudentServiceImpl;

@SpringBootTest
public class StudentServiceTest {
    @Mock
    private StudentRepository repo;

    @InjectMocks
    private StudentServiceImpl service;

    @Test
    public void whenGetAllStudents_thenReturnStudentList() {
        Student obj = new Student();
        obj.setId(1);
        obj.setName("Test Student");

        when(repo.findAll()).thenReturn(Collections.singletonList(obj));

        List<Student> result = service.getAll();

        assertEquals(1, result.size());
        assertEquals("Test Student", result.get(0).getName());
    }

    @Test
    public void whenGetStudentById_thenReturnStudent() {
        Student obj = new Student();
        obj.setId(1);
        obj.setName("Test Student");

        when(repo.findById(1)).thenReturn(Optional.of(obj));

        Student result = service.getById(1);

        assertEquals("Test Student", result.getName());
    }

    @Test
    public void whenNotFound() {

        when(repo.findById(1)).thenReturn(Optional.ofNullable(null));

        Student result = service.getById(1);

        assertNull(result);
    }

    @Test
    public void whenSaveStudent_thenStudentShouldBeSaved() {
        Student obj = new Student();
        obj.setId(1);
        obj.setName("Test Student");

        when(repo.save(any(Student.class))).thenReturn(obj);

        Student savedStudent = service.save(obj);

        assertNotNull(savedStudent);
        assertEquals(obj.getName(), savedStudent.getName());
        verify(repo, times(1)).save(obj);
    }

    @Test
    public void customSave() {
        Student obj = new Student();
        obj.setId(1);
        obj.setName("Test Student");
        obj.setSurname("Surname");
        obj.setBirthdate(LocalDate.of(2000, 3, 3));
        obj.setGender(Gender.FEMALE);

        service.saveCustom(obj);

        verify(repo, times(1)).saveCustom(obj.getName(), obj.getSurname(), obj.getBirthdate(), obj.getGender());
    }

    @Test
    public void whenDeleteStudent_thenStudentShouldBeDeleted() {
        int objId = 1;

        doNothing().when(repo).deleteById(objId);
        service.deleteById(objId);

        verify(repo, times(1)).deleteById(objId);
    }

    @Test
    public void whenUpdateStudent_thenStudentShouldBeUpdated() {
        Student existingStudent = new Student();
        existingStudent.setId(1);
        existingStudent.setName("Old Student Name");

        Student updated = new Student();
        updated.setId(1);
        updated.setName("Updated Student Name");

        when(repo.findById(existingStudent.getId())).thenReturn(Optional.of(existingStudent));
        when(repo.save(any(Student.class))).thenAnswer(i -> i.getArguments()[0]);

        Student updatedStudent = service.updateById(existingStudent.getId(), updated);

        assertNotNull(updatedStudent);
        assertEquals(updated.getName(), updatedStudent.getName());
        verify(repo, times(1)).save(updatedStudent);
    }

}
