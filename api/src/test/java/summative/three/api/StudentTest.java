
package summative.three.api;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Collections;

import org.junit.jupiter.api.Test;

import summative.three.api.models.Student;
import summative.three.api.models.Score;
import java.time.LocalDate;

public class StudentTest {
    @Test
    void testStudentGettersAndSetters() {
        Student obj = new Student();

        obj.setId(1);
        obj.setName("Test Student");
        obj.setSurname("Test Surname");
        obj.setBirthdate(LocalDate.of(2000, 3, 3));
        obj.setGender(Student.Gender.FEMALE);

        Score score = new Score();
        score.setId(1);
        obj.setScores(Collections.singletonList(score));

        assertEquals(1, obj.getId());
        assertEquals("Test Student", obj.getName());
        assertEquals("Test Surname", obj.getSurname());
        assertEquals(LocalDate.of(2000, 3, 3), obj.getBirthdate());
        assertEquals(Student.Gender.FEMALE, obj.getGender());
        assertEquals(1, obj.getScores().size());
        assertEquals(1, obj.getScores().get(0).getId());
    }
}
