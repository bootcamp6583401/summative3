package summative.three.api;

import summative.three.api.controllers.StudentController;
import summative.three.api.models.Student;
import summative.three.api.services.iml.StudentServiceImpl;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import java.util.List;
import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(StudentController.class)
public class StudentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StudentServiceImpl service;

    @Test
    public void getAllStudents_thenReturnJsonArray() throws Exception {
        Student student1 = new Student();
        student1.setId(1);
        student1.setName("Student1");

        Student student2 = new Student();
        student2.setId(2);
        student2.setName("Student2");

        List<Student> allStudents = Arrays.asList(student1, student2);

        given(service.getAll()).willReturn(allStudents);

        mockMvc.perform(get("/students")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].name", is(student1.getName())))
                .andExpect(jsonPath("$[1].name", is(student2.getName())));
    }

    @Test
    public void getStudentById_WhenStudentExists() throws Exception {
        int studentId = 1;
        Student student = new Student();
        student.setId(studentId);
        student.setName("Student1");

        given(service.getById(studentId)).willReturn(student);

        mockMvc.perform(get("/students/{id}", studentId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(student.getName())));
    }

    @Test
    public void createStudent_WhenPostStudent() throws Exception {
        Student student = new Student();
        student.setId(1);
        student.setName("Student1");

        given(service.save(any(Student.class))).willReturn(student);

        mockMvc.perform(post("/students")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"Student1\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(student.getName())));
    }

    @Test
    public void deleteStudent_WhenStudentExists() throws Exception {
        int studentId = 1;

        doNothing().when(service).deleteById(studentId);

        mockMvc.perform(delete("/students/{id}", studentId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void updateStudent_WhenPutStudent() throws Exception {
        int studentId = 1;
        Student student = new Student();
        student.setId(studentId);
        student.setName("Updated Student");

        given(service.updateById(eq(studentId), any(Student.class))).willReturn(student);

        mockMvc.perform(put("/students/{id}", studentId)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"Updated Student\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(student.getName())));
    }
}