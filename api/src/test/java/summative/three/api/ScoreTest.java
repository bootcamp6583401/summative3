
package summative.three.api;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

import summative.three.api.models.Student;
import summative.three.api.models.Lesson;
import summative.three.api.models.Score;

public class ScoreTest {
    @Test
    void testScoreGettersAndSetters() {
        Score score = new Score();
        score.setId(1);
        score.setScore(99);

        Student student = new Student();
        student.setId(1);
        score.setStudent(student);

        Lesson lesson = new Lesson();
        lesson.setId(1);
        score.setLesson(lesson);

        assertEquals(1, score.getId());
        assertEquals(99, score.getScore());
        assertNotNull(score.getStudent());
        assertEquals(1, score.getStudent().getId());
        assertEquals(1, score.getLesson().getId());
    }

}
