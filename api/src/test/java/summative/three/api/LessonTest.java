
package summative.three.api;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Collections;

import org.junit.jupiter.api.Test;

import summative.three.api.models.Lesson;
import summative.three.api.models.Score;

public class LessonTest {
    @Test
    void testLessonGettersAndSetters() {
        Lesson obj = new Lesson();

        obj.setId(1);
        obj.setLevel(3);
        obj.setName("Test Lesson");

        Score score = new Score();
        score.setId(1);
        obj.setScores(Collections.singletonList(score));

        assertEquals(1, obj.getId());
        assertEquals("Test Lesson", obj.getName());
        assertEquals(3, obj.getLevel());
        assertEquals(1, obj.getScores().size());
        assertEquals(1, obj.getScores().get(0).getId());
    }
}
