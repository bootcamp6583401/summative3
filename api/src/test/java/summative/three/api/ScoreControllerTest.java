package summative.three.api;

import summative.three.api.controllers.ScoreController;
import summative.three.api.models.Score;
import summative.three.api.services.iml.ScoreServiceImpl;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import java.util.List;
import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ScoreController.class)
public class ScoreControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ScoreServiceImpl service;

    @Test
    public void getAllScores_thenReturnJsonArray() throws Exception {
        Score score1 = new Score();
        score1.setId(1);
        score1.setScore(99);

        Score score2 = new Score();
        score2.setId(2);
        score2.setScore(98);

        List<Score> allScores = Arrays.asList(score1, score2);

        given(service.getAll()).willReturn(allScores);

        mockMvc.perform(get("/scores")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].score", is(score1.getScore())))
                .andExpect(jsonPath("$[1].score", is(score2.getScore())));
    }

    @Test
    public void getScoreById_WhenScoreExists() throws Exception {
        int scoreId = 1;
        Score score = new Score();
        score.setId(scoreId);
        score.setScore(99);

        given(service.getById(scoreId)).willReturn(score);

        mockMvc.perform(get("/scores/{id}", scoreId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.score", is(score.getScore())));
    }

    @Test
    public void createScore_WhenPostScore() throws Exception {
        Score score = new Score();
        score.setId(1);
        score.setScore(99);

        given(service.save(any(Score.class))).willReturn(score);

        mockMvc.perform(post("/scores")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"score\":3}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.score", is(score.getScore())));
    }

    @Test
    public void deleteScore_WhenScoreExists() throws Exception {
        int scoreId = 1;

        doNothing().when(service).deleteById(scoreId);

        mockMvc.perform(delete("/scores/{id}", scoreId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void updateScore_WhenPutScore() throws Exception {
        int scoreId = 1;
        Score score = new Score();
        score.setId(scoreId);
        score.setScore(3);

        given(service.updateById(eq(scoreId), any(Score.class))).willReturn(score);

        mockMvc.perform(put("/scores/{id}", scoreId)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"score\":3}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.score", is(score.getScore())));
    }
}