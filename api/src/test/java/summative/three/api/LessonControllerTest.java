package summative.three.api;

import summative.three.api.controllers.LessonController;
import summative.three.api.models.Lesson;
import summative.three.api.services.iml.LessonServiceImpl;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import java.util.List;
import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(LessonController.class)
public class LessonControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LessonServiceImpl service;

    @Test
    public void getAllLessons_thenReturnJsonArray() throws Exception {
        Lesson lesson1 = new Lesson();
        lesson1.setId(1);
        lesson1.setName("Lesson1");

        Lesson lesson2 = new Lesson();
        lesson2.setId(2);
        lesson2.setName("Lesson2");

        List<Lesson> allLessons = Arrays.asList(lesson1, lesson2);

        given(service.getAll()).willReturn(allLessons);

        mockMvc.perform(get("/lessons")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].name", is(lesson1.getName())))
                .andExpect(jsonPath("$[1].name", is(lesson2.getName())));
    }

    @Test
    public void getLessonById_WhenLessonExists() throws Exception {
        int lessonId = 1;
        Lesson lesson = new Lesson();
        lesson.setId(lessonId);
        lesson.setName("Lesson1");

        given(service.getById(lessonId)).willReturn(lesson);

        mockMvc.perform(get("/lessons/{id}", lessonId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(lesson.getName())));
    }

    @Test
    public void createLesson_WhenPostLesson() throws Exception {
        Lesson lesson = new Lesson();
        lesson.setId(1);
        lesson.setName("Lesson1");

        given(service.save(any(Lesson.class))).willReturn(lesson);

        mockMvc.perform(post("/lessons")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"Lesson1\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(lesson.getName())));
    }

    @Test
    public void deleteLesson_WhenLessonExists() throws Exception {
        int lessonId = 1;

        doNothing().when(service).deleteById(lessonId);

        mockMvc.perform(delete("/lessons/{id}", lessonId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void updateLesson_WhenPutLesson() throws Exception {
        int lessonId = 1;
        Lesson lesson = new Lesson();
        lesson.setId(lessonId);
        lesson.setName("Updated Lesson");

        given(service.updateById(eq(lessonId), any(Lesson.class))).willReturn(lesson);

        mockMvc.perform(put("/lessons/{id}", lessonId)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"Updated Lesson\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(lesson.getName())));
    }
}