package summative.three.api;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import summative.three.api.models.Lesson;
import summative.three.api.repositories.LessonRepository;
import summative.three.api.services.iml.LessonServiceImpl;

@SpringBootTest
public class LessonServiceTest {
    @Mock
    private LessonRepository repo;

    @InjectMocks
    private LessonServiceImpl service;

    @Test
    public void whenGetAllLessons_thenReturnLessonList() {
        Lesson obj = new Lesson();
        obj.setId(1);
        obj.setName("Test Lesson");

        when(repo.findAll()).thenReturn(Collections.singletonList(obj));

        List<Lesson> result = service.getAll();

        assertEquals(1, result.size());
        assertEquals("Test Lesson", result.get(0).getName());
    }

    @Test
    public void whenGetLessonById_thenReturnLesson() {
        Lesson obj = new Lesson();
        obj.setId(1);
        obj.setName("Test Lesson");

        when(repo.findById(1)).thenReturn(Optional.of(obj));

        Lesson result = service.getById(1);

        assertEquals("Test Lesson", result.getName());
    }

    @Test
    public void whenNotFound() {

        when(repo.findById(1)).thenReturn(Optional.ofNullable(null));

        Lesson result = service.getById(1);

        assertNull(result);
    }

    @Test
    public void whenSaveLesson_thenLessonShouldBeSaved() {
        Lesson obj = new Lesson();
        obj.setId(1);
        obj.setName("Test Lesson");

        when(repo.save(any(Lesson.class))).thenReturn(obj);

        Lesson savedLesson = service.save(obj);

        assertNotNull(savedLesson);
        assertEquals(obj.getName(), savedLesson.getName());
        verify(repo, times(1)).save(obj);
    }

    @Test
    public void customSave() {
        Lesson obj = new Lesson();
        obj.setId(1);
        obj.setName("Test Lesson");

        // when(repo.save(any(Lesson.class))).thenReturn(obj);

        service.saveCustom(obj);

        verify(repo, times(1)).saveCustom(obj.getName(), obj.getLevel());
    }

    @Test
    public void whenDeleteLesson_thenLessonShouldBeDeleted() {
        int objId = 1;

        doNothing().when(repo).deleteById(objId);
        service.deleteById(objId);

        verify(repo, times(1)).deleteById(objId);
    }

    @Test
    public void whenUpdateLesson_thenLessonShouldBeUpdated() {
        Lesson existingLesson = new Lesson();
        existingLesson.setId(1);
        existingLesson.setName("Old Lesson Name");

        Lesson updated = new Lesson();
        updated.setId(1);
        updated.setName("Updated Lesson Name");

        when(repo.findById(existingLesson.getId())).thenReturn(Optional.of(existingLesson));
        when(repo.save(any(Lesson.class))).thenAnswer(i -> i.getArguments()[0]);

        Lesson updatedLesson = service.updateById(existingLesson.getId(), updated);

        assertNotNull(updatedLesson);
        assertEquals(updated.getName(), updatedLesson.getName());
        verify(repo, times(1)).save(updatedLesson);
    }

}
