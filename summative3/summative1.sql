CREATE TABLE student (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    surname VARCHAR(255) NOT NULL,
    birthdate DATE,
    gender ENUM('MALE', 'FEMALE') NOT NULL
);

CREATE TABLE lesson (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    level INT NOT NULL
);

CREATE TABLE score (
    id INT AUTO_INCREMENT PRIMARY KEY,
    score INT NOT NULL,
    student_id INT NOT NULL,
    lesson_id INT NOT NULL,
    FOREIGN KEY (student_id) REFERENCES student(id),
    FOREIGN KEY (lesson_id) REFERENCES lesson(id)
);


INSERT INTO student (name, surname, birthdate, gender) VALUES
('Michael', 'Scott', '1964-03-15', 'MALE'),
('Dwight', 'Schrute', '1968-01-20', 'MALE'),
('Jim', 'Halpert', '1978-10-01', 'MALE'),
('Pam', 'Beesly', '1979-03-25', 'FEMALE'),
('Ryan', 'Howard', '1979-02-05', 'MALE'),
('Kelly', 'Kapoor', '1980-02-05', 'FEMALE'),
('Stanley', 'Hudson', '1958-02-19', 'MALE'),
('Angela', 'Martin', '1978-06-25', 'FEMALE'),
('Oscar', 'Martinez', '1968-02-19', 'MALE'),
('Phyllis', 'Vance', '1950-08-25', 'FEMALE');

INSERT INTO lesson (name, level) VALUES
('Sales Training', 10),
('Customer Service', 9),
('Accounting Principles', 6),
('Dunder Mifflin Paper Products', 7),
('Warehouse Management', 8);

INSERT INTO score (score, student_id, lesson_id) VALUES
(95, 1, 1),
(95, 1, 5),
(90, 1, 4),
(95, 2, 1),
(95, 2, 4),
(95, 3, 1),
(80, 3, 4),
(95, 4, 2),
(93, 5, 1),
(95, 5, 4),
(99, 6, 2),
(95, 7, 1),
(90, 7, 4),
(95, 8, 3),
(90, 9, 3),
(91, 10, 1),
(94, 10, 4);



