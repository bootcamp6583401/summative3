
-- DATA GABUNGAN
SELECT
    s.id AS student_id,
    s.name AS student_name,
    s.surname AS student_surname,
    s.birthdate AS student_birthdate,
    s.gender AS student_gender,
    l.id AS lesson_id,
    l.name AS lesson_name,
    sc.score AS lesson_score
FROM
    student s
JOIN
    score sc ON s.id = sc.student_id
JOIN
    lesson l ON sc.lesson_id = l.id
ORDER BY
    student_name;

-- Rata2 score lesson
SELECT
    l.id AS lesson_id,
    l.name AS lesson_name,
    AVG(sc.score) AS average_lesson_score
FROM
    lesson l
LEFT JOIN
    score sc ON l.id = sc.lesson_id
GROUP BY
    l.id, l.name;

-- Rata2 score student
SELECT
    s.id AS student_id,
    s.name AS student_name,
    AVG(sc.score) AS average_student_score
FROM
    student s
LEFT JOIN
    score sc ON s.id = sc.student_id
GROUP BY
    s.id, s.name;
